package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.AMMHome;
import ObjectRepositories.AMMLogin;
import ObjectRepositories.AMMMessageSearchPage;

public class AMMSearch {
	WebDriver driver = null;

	@BeforeTest
	public void Initializtion() throws InterruptedException {
		String browserName = "Firefox";
		if (browserName.equals("Firefox")) {
			System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
		} else if (browserName.equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "C:\\MyWorkspace\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}
		driver.get("https://netlink04.netlink-testlabs.com/swp/group/messagemgmt/#");
		

	}
	@Test
	public void MessageSearch() throws InterruptedException
	{
		AMMLogin ammLogin = new AMMLogin(driver);
		ammLogin.LoginOption().click();
		ammLogin.WaitFunction();
		ammLogin.Username().sendKeys("seleniumamm");
		ammLogin.Password().sendKeys("Feb2021Feb2021+");
		ammLogin.Login().click();
		AMMHome ammHome = new AMMHome(driver);
		ammHome.FileMsgSearch().click();
		AMMMessageSearchPage amsp=new AMMMessageSearchPage(driver);
		amsp.SearchAMMMessage();
		//amsp.NavigateHome();
		amsp.Logout();
	}

}
