package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.SoaphaPage;
import ObjectRepositories.SwiftToolsHome;

public class SwiftTools_soapha {
	WebDriver driver = null;

	@BeforeTest
	public void Initialization() throws InterruptedException {
		String browserName = "Firefox";
		if (browserName.equals("Firefox")) {
			System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
		} else if (browserName.equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "C:\\MyWorkspace\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}
		driver.get("http://192.168.5.40:8085/swifttools/index.xhtml");

	}

	@Test
	public void SendMessage() throws InterruptedException {
		SwiftToolsHome sth = new SwiftToolsHome(driver);
		sth.ClickTest("soapha");
		SoaphaPage sp = new SoaphaPage(driver);
		sp.SendSoapMessage();
		sp.VerifySoapMessageStatus();

	}

}
