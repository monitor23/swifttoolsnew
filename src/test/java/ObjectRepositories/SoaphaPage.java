package ObjectRepositories;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SoaphaPage {
	WebDriver driver;
	WebDriverWait wait;
	SimpleDateFormat date = new SimpleDateFormat("hhmmssss", Locale.ENGLISH);
	String currentDate = date.format(new Date());
	String ref = "SOAP-" + currentDate;
	String mRef = "MESSAGE TEXT" + currentDate;

	@SuppressWarnings("deprecation")
	public SoaphaPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void SendSoapMessage() throws InterruptedException {
		/*
		 * Thread.sleep(1000);
		 * 
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
		 * " #j_idt27\\:inputMode > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1) > div:nth-child(2)"
		 * ))) .click();
		 */
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt32:field20"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt32:field20"))).sendKeys(ref);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt32:field79"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_idt32:field79"))).sendKeys(mRef);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.ui-button-text:nth-child(1)")))
				.click();
		System.out.println("Test SOAPHA");
		System.out.println("-----------");
	}

	public void VerifySoapMessageStatus() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formSalida:resultsTable:j_idt66")));
		Thread.sleep(1500);
		for (int i = 1; i <= 4; i++) {
			String req = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.cssSelector("tr.ui-widget-content:nth-child(" + i + ") > td:nth-child(1)"))).getText();

			String status = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.cssSelector("tr.ui-widget-content:nth-child(" + i + ") > td:nth-child(2)"))).getText();

			if (req.equals("Open SOAPSEND") || req.equals("FIN message") || req.equals("Open SOAPGET")
					|| req.equals("Ack FIN")) {

				System.out.println("Method Name : " + req);
			} else {
				System.out.println("Method Name Not Found");
			}
			if (status.equals("200")) {
				System.out.println("Status Code : " + status);
			} else {
				System.out.println("Status Code : " + status);
			}

		}
		Thread.sleep(1500);
		driver.quit();
	}

}
