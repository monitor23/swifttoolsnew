package ObjectRepositories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AMMHome {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public AMMHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "gwt-debug-desktop-applications-com.swift.Access.samSearch")
	WebElement FileMsgSearch;

	public WebElement FileMsgSearch() {
		wait.until(ExpectedConditions.visibilityOf(FileMsgSearch));
		return FileMsgSearch;
	}

}
