package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SwiftToolsHome {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public SwiftToolsHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void ClickTest(String type) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector(".ui-helper-reset > li:nth-child(1) > a:nth-child(1)")))
				.click();
		Thread.sleep(1500);

		if (type.equals("soapha")) {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("li.ui-widget:nth-child(4) > a:nth-child(1)")));
			Actions a = new Actions(driver);
			a.moveToElement(driver.findElement(By.cssSelector("li.ui-widget:nth-child(4) > a:nth-child(1)"))).build()
					.perform();
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.cssSelector("li.ui-widget:nth-child(4) > ul:nth-child(2) > li:nth-child(1)"))).click();
		} else if (type.equals("MT199")) {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("li.ui-widget:nth-child(4) > a:nth-child(1)")));
			Actions a = new Actions(driver);
			a.moveToElement(driver.findElement(By.cssSelector("li.ui-widget:nth-child(4) > a:nth-child(1)"))).build()
					.perform();
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.cssSelector("li.ui-widget:nth-child(4) > ul:nth-child(2) > li:nth-child(2)"))).click();
		} else {
			System.out.println("Message Type not available");
		}
	}

	public void ClickAGI(String type) throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector(".ui-helper-reset > li:nth-child(1) > a:nth-child(1)")))
				.click();
		Thread.sleep(1500);
		if (type.equals("AGI")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.cssSelector(".ui-menuitem-active > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)")))
					.click();
		}

	}

	public void ClickMQ() throws InterruptedException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector(".ui-helper-reset > li:nth-child(1) > a:nth-child(1)")))
				.click();
		Thread.sleep(1500);

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("li.ui-widget:nth-child(3) > a:nth-child(1)")));
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.cssSelector("li.ui-widget:nth-child(3) > a:nth-child(1)"))).build()
				.perform();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("li.ui-widget:nth-child(3) > ul:nth-child(2) > li:nth-child(2)"))).click();

	}

}
