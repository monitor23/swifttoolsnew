package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AMMMessageSearchPage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public AMMMessageSearchPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 180);
		PageFactory.initElements(driver, this);
	}

	public void SearchAMMMessage() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-Alliance_Access_Entry-frame")));
		driver.switchTo().frame("gwt-debug-Alliance_Access_Entry-frame");
		Thread.sleep(6000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("gwt-debug--messenger-messageSearch-criteria-actionSearch"))).click();
		Thread.sleep(6000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("gwt-debug--messenger-messageSearch-criteria-actionSearch"))).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("gwt-debug--messenger-messageSearch-criteria-actionClear"))).click();
		driver.switchTo().defaultContent();		
	}

	public void NavigateHome() throws InterruptedException {
		driver.switchTo().defaultContent();
		Thread.sleep(4000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-desktop-mainmenu-Home"))).click();
	}

	public void Logout() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("#gwt-debug-desktop-topmenu-Logout > div:nth-child(2) > div:nth-child(1)"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-dialog-ask-0-ok"))).click();
		driver.quit();
	}

}
